# Predictive Testing with Machine Learning in Software Regression Tests

### Requirements
+ Python 3.9
+ pipenv installed

### Installation
Make sure you're in predictive-testing-masterthesis folder, then:

```pipenv install```

### create testdata to project
1. create project config from template in src/config
2. in .env set PROJECT_TO_TEST variable to set which project should be tested.
3. execute src/main

### visualization / data analysis / machine learning
1. startup jupyter lab "pipenv run jupyter lab"
2. execute desired notebook, change config in notebook
