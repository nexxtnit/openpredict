"""
Description:
    this tool is for initialising and manipulating a database for the testdata.
"""
from typing import MutableMapping, Union
from loguru import logger
from pathlib import Path
import sqlite3
from sqlite3.dbapi2 import Connection, Cursor, Error
from tqdm import tqdm


class DataBaseTest:
    def __init__(self, config: MutableMapping):
        # init logger
        self.logger = logger.bind(machine="host")
        self.logger = self.logger.bind(DA=True)

        self.main_dir_path = Path(__file__).parent.parent.parent.resolve()
        self.database_path: Path = (
            self.main_dir_path
            / config["build"]["path"]
            / config["database"]["file"]
        )
        self.storage_path = self.main_dir_path / "database"
        self.database: Connection = None
        self.cursor: Cursor = None

    ############################
    ##### PUBLIC FUNCTIONS
    ############################

    def open_database(self) -> bool:
        """
        create a database connection to the SQLite database
        specified by db_file
        :param db_file: database file
        :return: Connection object or None
        """
        try:
            if self.database is None:
                self.database = sqlite3.connect(self.database_path)
                self.cursor = self.database.cursor()
            return True
        except Error as e:
            print(e)
            return False

    def close_database(self):
        self.database.close()
        self.database = None

    def initialize_database(self):
        """initialize empty database tables"""
        self.open_database()
        commit_table = [
            "CommitID INTEGER PRIMARY KEY",
            "CommitHash TEXT NOT NULL",
            "CommitTag TEXT",
        ]
        self.insert_new_table("Commits", commit_table)

        file_diff_table = [
            "CommitID INTEGER NOT NULL",
            "File TEXT",
            "Change TEXT",
            "FileComplexity INTEGER",  # Cyclomatic Complexity of the file
            "AddedLines INTEGER",
            "DeletedLines INTEGER",
            "LinesOfCode INTEGER",
            "FilePath TEXT",
            "FOREIGN KEY(CommitID) REFERENCES Commits(CommitID)",
        ]
        self.insert_new_table("FileDiff", file_diff_table)

        file_tests_table = [
            "TestID INTEGER PRIMARY KEY",
            "TestName TEXT NOT NULL ",
            "TestPath TEXT",
            "CommitID INTEGER",
            "FOREIGN KEY(CommitID) REFERENCES Commits(CommitID)",
        ]
        self.insert_new_table("TestSuite", file_tests_table)

        file_tests_table = [
            "CommitID INTEGER NOT NULL",
            "TestName TEXT NOT NULL ",
            "Passed INTEGER ",
            "Failed INTEGER ",
            "Skipped INTEGER ",
            "FOREIGN KEY(CommitID) REFERENCES Commits(CommitID)",
            "FOREIGN KEY(TestName) REFERENCES TestSuite(TestName)",
        ]
        self.insert_new_table("TestRun", file_tests_table)
        self.close_database()

    ########################
    ### WRITE OPERATIONS ###
    ########################

    def insert_new_table(self, table_name: str, table_data: list[str]):
        """insert new table in tabase

        Args:
            table_name (str)
            table_data (list[str])
        """
        sql_cmd = f"CREATE TABLE IF NOT EXISTS {table_name} ("
        sql_cmd += ",".join(table_data)
        sql_cmd.rstrip(",")
        sql_cmd += ");"
        self.execute_sql_cmd(sql_cmd)

    def insert_commits(self, commits: list[str], tags: list[str]) -> None:
        """
        insert row |commit | tag |
        """
        self.logger.info("fill table <Commits> with git-hash and git-tag")
        self.open_database()
        for i in tqdm(range(len(commits))):
            self.__insert_new_commit(commits[i], tags[i])
        self.close_database()

    def insert_new_diff(
        self,
        commit_hash: str,
        file: str,
        change: str,
        fileComplexity: int,
        addLines: int,
        delLine: int,
        loc: int,
        filepath: str,
    ):
        """
        insert new row | commit_hash | file that changed | change
            Database needs to be open !
        """
        if not self.__check_diff_exists(commit_hash, file):
            commit_id = self.get_commitid_of_commithash(commit_hash)
            self.__insert_to_table(
                "FileDiff",
                [
                    str(commit_id),
                    f"'{file}'",
                    f"'{change}'",
                    str(fileComplexity),
                    str(addLines),
                    str(delLine),
                    str(loc),
                    f"'{filepath}'",
                ],
            )

    def insert_tests_to_testsuite(
        self, test_name: str, test_path: str, commitID: int
    ) -> None:
        """
        Insert a test into the testsuite databse
        """
        self.__insert_to_table(
            "TestSuite",
            [
                "NULL",
                f"'{test_name}'",
                f"'{test_path}'",
                f"{commitID}",
            ],
        )

    def insert_new_testresult(
        self,
        commit_id: int,
        testname: str,
        passed: int,
        failed: int,
        skipped: int,
    ):
        """
        add new testresult from testrun
        """
        self.__insert_to_table(
            "TestRun",
            [
                str(commit_id),
                f" '{testname}'",
                f"'{passed}'",
                f"'{failed}'",
                f"'{skipped}'",
            ],
        )

    def drop_testrun_by_commit(self, commitID):
        """drop a testrun because it is invalid

        Args:
            commitID (_type_)
        """
        sql_cmd = f"DELETE FROM TestRun WHERE CommitID={commitID};"
        self.execute_sql_cmd(sql_cmd)

    def drop_table(self, table_name: str):
        """drop selected table

        Args:
            table_name (str):
        """
        sql_cmd = f"DROP TABLE IF EXISTS {table_name};"
        self.execute_sql_cmd(sql_cmd)

    #######################
    ### READ OPERATIONS ###
    #######################

    def check_commit_exists(self, commit_hash: str) -> bool:
        """check commit already exists in database


        Args:
            commit_hash (str)

        Returns:
            bool: true if commit exists
        """
        commit_id = self.get_commitid_of_commithash(commit_hash)
        return bool(commit_id or commit_id is None)

    def get_testresult_for_commit(
        self, commitID: int
    ) -> list[tuple[int, str, str]]:
        """get result from testrun table for commitID

        Args:
            commitID (int)

        Returns:
            list[tuple[int, str, str]]: List of testresults
        """
        sql_cmd = "SELECT * from TestRun " + f"WHERE CommitID = {commitID}"
        if self.check_commit_exists(self.get_commithash_of_commitid(commitID)):
            try:
                return self.execute_sql_cmd(sql_cmd)
            except (TypeError):
                return []
        self.logger.error(f"CommitID: {commitID} does not exists")

    def get_tests_failed_for_commit(
        self, commitID: int
    ) -> list[tuple[int, str, str]]:
        """get testnames that failed in table testrun for certain commitID

        Args:
            commitID (int)

        Returns:
            list[tuple[int, str, str]]: list of failed tests
        """
        sql_cmd = (
            "SELECT TestName from TestRun "
            + f"WHERE CommitID = {commitID} "
            + "AND Result = 'F' "
        )
        if self.check_commit_exists(self.get_commithash_of_commitid(commitID)):
            try:
                return [tuple[0] for tuple in self.execute_sql_cmd(sql_cmd)]
            except (TypeError):
                return []
        self.logger.error(f"CommitID: {commitID} does not exists")

    def get_testrun(self) -> list[tuple[int, str, str]]:
        """get testrun table

        Returns:
            list[tuple[int, str, str]]:
        """
        sql_cmd = "SELECT * from TestRun"
        return self.execute_sql_cmd(sql_cmd)

    def check_table_exists(self, tablename: str) -> bool:
        """check if table exists

        Args:
            tablename (str)

        Returns:
            bool:
        """
        sql_cmd = f"SELECT count(*) FROM sqlite_master WHERE type='table' AND name='{tablename}' ;"
        return self.execute_sql_cmd(sql_cmd)

    def get_testsuite(self) -> list[tuple[int, str, str]]:
        """get table testsuite

        Returns:
            list[tuple[int, str, str]]
        """
        if self.check_table_exists("TestSuite"):
            sql_cmd = "SELECT * from TestSuite;"
            output = self.execute_sql_cmd(sql_cmd)
            if output is None:
                return []
            return output
        return []

    def get_commitid_of_commithash(self, commit_hash: str) -> int:
        """return commit_id giving commit_hash


        Args:
            commit_hash (str)

        Returns:
            int: commitID
        """
        sql_cmd = (
            f"SELECT CommitID FROM Commits WHERE CommitHash = '{commit_hash}';"
        )
        output = self.execute_sql_cmd(sql_cmd)
        if not output:
            return 0
        if len(output) > 1:
            self.logger.error("two commit IDs having same hash")
            return None
        return output[0][0]

    def get_commithash_of_commitid(self, commit_id) -> str:
        """return commit_hash giving commit_id


        Args:
            commit_id (_type_):

        Returns:
            str: commithash
        """
        sql_cmd = (
            f"SELECT CommitHash FROM Commits WHERE CommitID = {commit_id};"
        )
        output = self.execute_sql_cmd(sql_cmd)
        if not output:
            return 0
        if len(output) > 1:
            self.logger.error("two commit IDs having same hash")
            return None
        return output[0][0]

    def get_all_diff_of_commit(self, commit_hash) -> Union[list[str], None]:
        """return all file changes giving commit_hash

        Args:
            commit_hash (_type_)

        Returns:
            Union[list[str], None]: file changes
        """
        commit_id = str(self.get_commitid_of_commithash(commit_hash))
        sql_cmd = f"SELECT File FROM FileDiff WHERE CommitID = {commit_id};"
        output = self.execute_sql_cmd(sql_cmd)
        if output is None:
            return None

        return [e[0] for e in output]

    def get_all_commits_of_filepath(self, filepath: str) -> list[int]:
        """return all commitIDs of commits changing file in given filepath
        used for finding all tests

        Args:
            filepath (str):

        Returns:
            list[int]:
        """
        sql_cmd = f"SELECT CommitID FROM FileDiff WHERE FilePath = '{filepath}' AND Change IS NOT 'DELETE' AND Change IS NOT 'RENAME';"
        output = self.execute_sql_cmd(sql_cmd)
        if output is None:
            return None
        return [e[0] for e in output]

    def get_all_testnames_and_paths_from_filediff(
        self,
    ) -> list[tuple[str, str]]:
        """return all testnames and testpathes for all tests in repository

        Returns:
            list[tuple[int, str, str]]: testname and testpath
        """
        sql_cmd = (
            "SELECT File,FilePath FROM FileDIFF WHERE "
            + "(File GLOB 'test_*' AND Change IS NOT 'DELETE' AND Change IS NOT 'RENAME') "
            + "OR (File GLOB '*_test.py' AND Change IS NOT 'DELETE' AND Change IS NOT 'RENAME') "
            + "OR (File GLOB 'test*.py' AND Change IS NOT 'DELETE' AND Change IS NOT 'RENAME')"
        )
        return self.execute_sql_cmd(sql_cmd)

    def get_test_path_of_testname(
        self, testname: str
    ) -> Union[str, bool, None]:
        """
        return test_path giving test_name
        """
        if not self.check_test_exists(testname):
            return None
        sql_cmd = (
            f"SELECT TestPath FROM TestSuite WHERE TestName = '{testname}';"
        )

        output = self.execute_sql_cmd(sql_cmd)
        if not output:
            return 0
        if len(output) > 1:
            self.logger.error("two tests having same Testname")
            return None
        return output[0][0]

    def get_testresult(
        self, commit_id: str, testname: str
    ) -> list[int, int, int]:
        """get testresult for test in certain commit

        Args:
            commit_id (str)
            testname (str)

        Returns:
            list[int, int, int]: results
        """
        sql_cmd = (
            "SELECT Result FROM TestRun"
            + f" WHERE CommitID = '{commit_id}' AND TestName ='{testname}';"
        )
        output = self.execute_sql_cmd(sql_cmd)
        return output

    #############################
    ##### PRIVATE FUNCTIONS
    #############################

    def __insert_new_commit(self, commit_hash: str, commit_tag: str):
        """
        Insert new commit with tag to table <Commits> in database
        """
        if not self.check_commit_exists(commit_hash):
            self.__insert_to_table(
                "Commits", ["NULL", f"'{commit_hash}'", f"'{commit_tag}'"]
            )

    def __check_database_isopen(self) -> bool:
        """
        check if database is open
        """
        if self.database is None:
            return self.open_database()
        return True

    def execute_sql_cmd(self, sql_cmd: str) -> Union[None, list[tuple]]:
        """execute sql md

        Args:
            sql_cmd (str): 

        Raises:
            e: when there is a problem with sql database

        Returns:
            Union[None, list[tuple]]
        """
        if self.__check_database_isopen():
            try:
                self.cursor.execute(sql_cmd)
                output = self.cursor.fetchall()
                self.database.commit()
                if output:
                    return output
            except Exception as e:
                self.logger.error(f"SQL command: [{sql_cmd}] produced an error")
                raise e

    def __insert_to_table(self, table_name: str, data: list[str]) -> None:
        sql_cmd = f"INSERT INTO {table_name} VALUES ("
        sql_cmd += ",".join(data)
        sql_cmd += ");"
        self.execute_sql_cmd(sql_cmd)

    def __check_diff_exists(self, commit_hash: str, file: str) -> bool:
        diff_files = self.get_all_diff_of_commit(commit_hash)
        if diff_files is None:
            return False
        return any(file in s for s in diff_files)
