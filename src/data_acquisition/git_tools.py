"""
Description:
    this tools are for gathering a git history of a project and present the repository information
    in a usefull manner.
"""

from pathlib import Path
from loguru import logger
from pydriller.domain.commit import Commit
from src.global_tools import system
from src.global_tools.config import global_config as config
from tqdm import tqdm
from pydriller import Repository


class ProjectRepository:
    def __init__(self, repo_link: str, start_commit: str):
        # init logger
        self.logger = logger.bind(machine="host")
        self.logger = self.logger.bind(DA=True)

        # init repo
        self.link = repo_link  # ssh-link: git@github.com:testrepo/testrepo.git
        self.start_commit = start_commit
        self.directory: Path = (
            Path.cwd() / config["build"]["path"] / config["repo"]["path"]
        )
        self.directory.mkdir(parents=True, exist_ok=True)
        self.repository = Repository(self.link)

    ############################
    ##### PUBLIC FUNCTIONS
    ############################

    def checkout_commit(self, commit_hash: str):
        self.logger.debug(f"checkout commit {commit_hash}")
        current_hash = self.get_current_commit()
        if commit_hash == current_hash:
            return
        self.git_cmd_run(["checkout", "-f", commit_hash], no_output=True)

    def get_current_commit(self) -> str:
        """returns current commit hash"""
        return self.git_cmd_run(["rev-parse", "HEAD"], no_output=True)[0]

    def get_diff_of_commits(self, old_commit_hash: str, new_commit_hash: str):
        return self.git_cmd_run(
            ["diff", "--name-status", old_commit_hash, new_commit_hash],
            no_output=True,
        )

    def get_tags(self, commits: list[str]) -> list[str]:
        self.logger.info("collect git tags of commits")
        commit_tags = []
        for hash in tqdm(commits):
            commit_tag = self.git_cmd_run(
                ["describe", "--tags", "--always", hash], no_output=True
            )
            commit_tags.append(commit_tag[0])
        return commit_tags

    #############################
    ##### PRIVATE FUNCTIONS
    #############################
    def create_repository_object(self, repo_path: str) -> None:
        self.repository = Repository(repo_path)

    def config_git(self) -> None:
        # turn off detached head info
        self.git_cmd_run(["config", "advice.detachedHead", "false"])

    def download_repo(self) -> None:
        self.logger.info(
            f"download {self.link.__str__()} into {self.directory.__str__()}"
        )
        self.git_cmd_run(["clone", self.link, self.directory.__str__()])

    def get_commit_objs(self) -> list[Commit]:
        """
        return lsit of commit hashes
        """
        self.logger.info("import commits of repo")
        return [commit for commit in self.repository.traverse_commits()]


    def git_cmd_run(self, command: list[str], no_output=False):
        """
        run git command in project_repo directory
        """
        command.insert(0, "git")
        return self.__cmd_run(command, self.directory, no_output)

    def __cmd_run(
        self, command: list[str], exec_location: str = None, no_output=False
    ) -> list[str]:
        """
        run command with own logger
        """
        return system.cmd_run(command, self.logger, exec_location, no_output)
